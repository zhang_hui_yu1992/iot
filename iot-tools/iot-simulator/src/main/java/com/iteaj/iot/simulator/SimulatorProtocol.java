package com.iteaj.iot.simulator;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.protocol.ClientInitiativeProtocol;

/**
 * 模拟器协议
 */
public abstract class SimulatorProtocol extends ClientInitiativeProtocol<SimulatorClientMessage> {

    @Override
    public SimulatorConnectProperties getClientKey() {
        ClientConnectProperties clientKey = super.getClientKey();
        if(clientKey == null && requestMessage() != null) {
            clientKey = requestMessage().getProperties();
        }

        return (SimulatorConnectProperties) clientKey;
    }
}
