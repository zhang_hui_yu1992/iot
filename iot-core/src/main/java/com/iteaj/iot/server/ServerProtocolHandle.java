package com.iteaj.iot.server;

import com.iteaj.iot.ProtocolHandle;

public interface ServerProtocolHandle<T extends ServerSocketProtocol> extends ProtocolHandle<T> {

}
