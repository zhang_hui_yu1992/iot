package com.iteaj.iot.server.udp;

import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.udp.UdpMessage;
import com.iteaj.iot.udp.UdpMessageBody;
import com.iteaj.iot.udp.UdpMessageHead;

import java.net.InetSocketAddress;

public abstract class UdpServerMessage extends ServerMessage implements UdpMessage {

    private InetSocketAddress sender;
    private InetSocketAddress recipient;

    public UdpServerMessage(byte[] message) {
        super(message);
    }

    /**
     * 报文头必须指定设备编号
     * @see UdpMessageHead#getEquipCode()
     * @param head
     */
    public UdpServerMessage(UdpMessageHead head) {
        super(head);
    }

    /**
     * 报文头必须指定设备编号
     * @see UdpMessageHead#getEquipCode()
     * @param head
     * @param body
     */
    public UdpServerMessage(UdpMessageHead head, UdpMessageBody body) {
        super(head, body);
    }

    public UdpServerMessage(byte[] message, InetSocketAddress recipient) {
        this(new UdpMessageHead(message), recipient);
    }

    public UdpServerMessage(UdpMessageHead head, InetSocketAddress recipient) {
        super(head);
        this.recipient = recipient;
    }

    public UdpServerMessage(UdpMessageHead head, UdpMessageBody body, InetSocketAddress recipient) {
        super(head, body);
        this.recipient = recipient;
    }

    public UdpServerMessage(UdpMessageHead head, InetSocketAddress sender, InetSocketAddress recipient) {
        super(head);
        this.sender = sender;
        this.recipient = recipient;
    }

    public UdpServerMessage(UdpMessageHead head, UdpMessageBody body, InetSocketAddress sender, InetSocketAddress recipient) {
        super(head, body);
        this.sender = sender;
        this.recipient = recipient;
    }

    @Override
    protected abstract UdpMessageHead doBuild(byte[] message);

    /**
     * 由于udp没有连接的概念, 所有的channelId都是服务端{@link io.netty.channel.socket.nio.NioDatagramChannel}的channelId
     * @return
     */
    @Override
    public String getChannelId() {
        return super.getChannelId();
    }

    @Override
    public InetSocketAddress getSender() {
        return sender;
    }

    @Override
    public UdpServerMessage setSender(InetSocketAddress sender) {
        this.sender = sender;
        return this;
    }

    @Override
    public InetSocketAddress getRecipient() {
        return recipient;
    }

    @Override
    public UdpServerMessage setRecipient(InetSocketAddress recipient) {
        this.recipient = recipient;
        return this;
    }

    @Override
    public UdpMessageHead getHead() {
        return (UdpMessageHead) super.getHead();
    }

    @Override
    public UdpMessageBody getBody() {
        return (UdpMessageBody) super.getBody();
    }
}
