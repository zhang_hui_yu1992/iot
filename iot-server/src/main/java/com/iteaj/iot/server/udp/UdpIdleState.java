package com.iteaj.iot.server.udp;

import com.iteaj.iot.config.ConnectProperties;
import io.netty.util.concurrent.ScheduledFuture;

import java.net.InetSocketAddress;

public class UdpIdleState {

    /**
     * 设备编号
     */
    private String equipCode;

    /**
     * 注册时毫秒数
     */
    private long registerOfMills;

    /**
     * 最新上报得毫秒数
     */
    private long lastOfMills;

    /**
     * 服务端连接配置
     */
    private ConnectProperties config;

    /**
     * 设备ip地址
     */
    private InetSocketAddress address;

    /**
     * 定时调度
     */
    private ScheduledFuture scheduledFuture;

    public UdpIdleState(String equipCode, long registerOfMills
            , ConnectProperties config, InetSocketAddress address) {
        this.config = config;
        this.address = address;
        this.equipCode = equipCode;
        this.lastOfMills = registerOfMills;
        this.registerOfMills = registerOfMills;
    }

    public String getEquipCode() {
        return equipCode;
    }

    public void setEquipCode(String equipCode) {
        this.equipCode = equipCode;
    }

    public long getRegisterOfMills() {
        return registerOfMills;
    }

    public void setRegisterOfMills(long registerOfMills) {
        this.registerOfMills = registerOfMills;
    }

    public ConnectProperties getConfig() {
        return config;
    }

    public void setConfig(ConnectProperties config) {
        this.config = config;
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public void setAddress(InetSocketAddress address) {
        this.address = address;
    }

    public ScheduledFuture getScheduledFuture() {
        return scheduledFuture;
    }

    public UdpIdleState setScheduledFuture(ScheduledFuture scheduledFuture) {
        this.scheduledFuture = scheduledFuture;
        return this;
    }

    public long getLastOfMills() {
        return lastOfMills;
    }

    public void setLastOfMills(long lastOfMills) {
        this.lastOfMills = lastOfMills;
    }
}
