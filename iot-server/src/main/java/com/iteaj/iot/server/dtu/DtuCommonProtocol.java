package com.iteaj.iot.server.dtu;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.server.dtu.message.DtuMessage;

public interface DtuCommonProtocol<M extends DtuMessage> extends Protocol {

    @Override
    M requestMessage();

    @Override
    M responseMessage();
}
