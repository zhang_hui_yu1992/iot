package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.modbus.server.tcp.ModbusTcpBody;
import com.iteaj.iot.modbus.server.tcp.ModbusTcpHeader;
import com.iteaj.iot.modbus.client.tcp.ModbusTcpClientMessage;

public class ModbusTcpForDtuClientTestMessage extends ModbusTcpClientMessage {

    public ModbusTcpForDtuClientTestMessage(byte[] message) {
        super(message);
    }

    public ModbusTcpForDtuClientTestMessage(ModbusTcpHeader head, ModbusTcpBody body) {
        super(head, body);
    }
}
